import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyFoodGUI {
    private JTabbedPane tabbedPane1;
    private JPanel root;
    private JButton fruitsandwichButton;
    private JButton shironoirButton;
    private JButton curryButton;
    private JButton hamburgerButton;
    private JButton fishburgerButton;
    private JButton colaButton;
    private JButton coffeeButton;
    private JButton teaButton;
    private JTextPane priceList;
    private JTextPane orderedItemsList;
    private JLabel total;
    private JButton resetButton;
    private JButton checkOutButton;
    private JLabel setCount;
    private JButton hotdogButton;
    private int sum = 0;
    private int setFoodCount = 0;
    private int setDrinkCount = 0;
    private int numberOfSet = 0;
    public MyFoodGUI() {

        curryButton.setIcon(new ImageIcon(
                this.getClass().getResource("curry.jpg")
        ));
        curryButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderFood("Curry",440);
            }
        });

        hamburgerButton.setIcon(new ImageIcon(
                this.getClass().getResource("hamburger.jpg")
        ));
        hamburgerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderFood("Hamburger",520);
            }
        });

        fishburgerButton.setIcon(new ImageIcon(
                this.getClass().getResource("fishburger.jpg")
        ));
        fishburgerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderFood("Fish burger",500);
            }
        });

        hotdogButton.setIcon(new ImageIcon(
                this.getClass().getResource("hotdog.jpg")
        ));
        hotdogButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderFood("Hot dog",400);
            }
        });

        fruitsandwichButton.setIcon(new ImageIcon(
                this.getClass().getResource("fruitsand.jpg")
        ));
        fruitsandwichButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderFood("Fruit sandwich",600);
            }
        });

        shironoirButton.setIcon(new ImageIcon(
                this.getClass().getResource("shironoir.jpg")
        ));
        shironoirButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderFood("Shironoir",550);
            }
        });

        colaButton.setIcon(new ImageIcon(
                this.getClass().getResource("cola.jpg")
        ));
        colaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderDrink("Cola",200);
            }
        });

        coffeeButton.setIcon(new ImageIcon(
                this.getClass().getResource("coffee.jpg")
        ));
        coffeeButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderDrink("Coffee",250);
            }
        });

        teaButton.setIcon(new ImageIcon(
                this.getClass().getResource("tea.jpg")
        ));
        teaButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                orderDrink("Tea",240);
            }
        });

        resetButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (sum == 0) {
                    JOptionPane.showMessageDialog(null, "Please select any food or drink.", "Not selected", 1);
                }
                else {
                    int confirmation = JOptionPane.showConfirmDialog(null,
                            "Would you like to reset?",
                            "Reset Confirmation",
                            JOptionPane.YES_NO_OPTION);
                    if (confirmation == 0) {
                        JOptionPane.showMessageDialog(null, "All items canceled");
                        setReset();
                        sum = 0;
                        total.setText("  Total            " + sum + " yen");
                        orderedItemsList.setText("");
                        priceList.setText("");
                    }
                }
            }
        });

        checkOutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if(sum==0){
                    JOptionPane.showMessageDialog(null, "Please select any food or drink.","Not selected",1);
                }
                else{
                    int confirmation = JOptionPane.showConfirmDialog(null,
                        "Would you like to checkout?",
                        "Order Confirmation",
                        JOptionPane.YES_NO_OPTION);
                    if (confirmation == 0){
                        JOptionPane.showMessageDialog(null, "Thank you! The total price is " + sum + " yen.");
                        setReset();
                        sum=0;
                        total.setText("  Total            " + sum + " yen");
                        orderedItemsList.setText("");
                        priceList.setText("");
                    }
                }
            }
        });
    }

    public static void main(String[] args) {
        JFrame frame = new JFrame("MyFoodGUI");
        frame.setContentPane(new MyFoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }


    void orderFood(String food,int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0){
            JOptionPane.showMessageDialog(null, "Order for " + food + " received.","Order received",1);
            setFoodCount+=1;
            if(setFoodCount * setDrinkCount > 0){
                sum += price - 100;
                setCount();
            }
            else{
                sum+=price;
            }
            total.setText("  Total            " + sum + " yen");
            String currentText = orderedItemsList.getText();
            orderedItemsList.setText(currentText + food + "\n");
            String orderedItemPrice = priceList.getText();
            priceList.setText(orderedItemPrice + "                    " + price + "\n");
        }
    }

    void orderDrink(String drink,int price){
        int confirmation = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + drink + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);
        if (confirmation == 0){
            JOptionPane.showMessageDialog(null, "Order for " + drink + " received.","Order received",1);
            setDrinkCount+=1;
            if(setFoodCount * setDrinkCount > 0){
                sum += price - 100;
                setCount();
            }
            else{
                sum += price;
            }
            total.setText("  Total            " + sum + " yen");
            String currentText = orderedItemsList.getText();
            orderedItemsList.setText(currentText + drink + "\n");
            String orderedItemPrice = priceList.getText();
            priceList.setText(orderedItemPrice + "                    " + price + "\n");
        }
    }

    void setCount(){
        setFoodCount -= 1;
        setDrinkCount -= 1;
        numberOfSet += 1;
        setCount.setText("Number of drinkset is " + numberOfSet);
    }

    void setReset(){
        setFoodCount = 0;
        setDrinkCount = 0;
        numberOfSet = 0;
        setCount.setText("Number of drinkset is " + numberOfSet);
    }
}
